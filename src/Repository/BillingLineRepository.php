<?php

namespace App\Repository;

use App\Entity\Pricing;
use App\Entity\BillingLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BillingLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillingLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillingLine[]    findAll()
 * @method BillingLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillingLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BillingLine::class);
    }

    /**
     * @return BillingLine[] Returns an array of BillingLine objects
     */
    
    public function findPricings($bill_Id)
    {
        return $this->createQueryBuilder('p')
            ->from('BillingLine','bl')
            ->join('Pricing','pr','ON', 'bl.pricing_ref = pr.reference')
            ->andWhere('p.billing = :id')
            ->setParameter('id', $bill_Id)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?BillingLine
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
