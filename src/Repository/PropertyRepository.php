<?php

namespace App\Repository;

use App\Entity\Property;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }

    public function filterProp(?int $type, DateTime $date_start, DateTime $date_end): array
    {
        $entityManager = $this->getEntityManager();

        if ($type != null) {

                $query = $entityManager->createQuery(
                'SELECT prop FROM App\Entity\Property prop 
                JOIN App\Entity\Pricing price 
                WITH price.id = prop.pricing 
                JOIN App\Entity\Type ty
                WITH ty.id = price.type
                WHERE price.type = :type
                AND prop.availability_start <= :datestart
                AND prop.availability_end >= :dateend
                ORDER BY prop.id ASC'
            )
            ->setParameters([
                'type' => $type,
                'datestart' => $date_start,
                'dateend' => $date_end
            ]);

        }else{

            $query = $entityManager->createQuery(
                'SELECT prop FROM App\Entity\Property prop 
                JOIN App\Entity\Pricing price 
                WITH price.id = prop.pricing 
                JOIN App\Entity\Type ty
                WITH ty.id = price.type
                WHERE prop.availability_start <= :datestart
                AND prop.availability_end >= :dateend
                ORDER BY prop.id ASC'
            )
            ->setParameters([
                'datestart' => $date_start,
                'dateend' => $date_end
            ]);
        }

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findPricing(int $id): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT prop FROM App\Entity\Property prop
            JOIN App\Entity\Pricing price
            WITH price.id = prop.pricing
            WHERE prop.id = :id;'
        )->setParameter('id', $id); 

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findPropertyByOwner(int $id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.owner = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Property[] Returns an array of Property objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Property
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
