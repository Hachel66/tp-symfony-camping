<?php

namespace App\Form;

use App\Entity\Pricing;
use App\Entity\Property;
use App\Entity\User;
use App\Repository\PricingRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom de la propriété :'
            ])
            ->add('availability_start', DateType::class, [
                'label' => "Début de disponibilité : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('availability_end', DateType::class, [
                'label' => "Fin de disponibilité : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('owner', EntityType::class, [
                'class' => User::class,
                'label' => 'Appartient à :',
                'choice_label' => "fullname",
                'placeholder' => '---',
                'multiple' => false,
                'query_builder' => function (UserRepository $repoUser) {
                    return $repoUser->findPartUsers();
                }
                
            ])
            ->add('pricing', EntityType::class, [
                'class' => Pricing::class,
                'label' => "Type de propriété: ",
                'placeholder' => '---',
                'choice_label' => 'label',
                'multiple' => false,
                'query_builder' => function (PricingRepository $repoPrice) {
                    return $repoPrice->findPrimaryPricings();
                }
            ])
            ->add('submit', SubmitType::class, ['label' => 'Valider']);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class,
        ]);
    }
}
