<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add( 'firstname', null, [
            'label' => 'Prénom :'
        ])
            ->add( 'lastname', null, [
            'label' => 'Nom :'
        ])
            ->add( 'password', PasswordType::class, [
            'label' => 'Mot de passe :'
        ])
            ->add( 'address', null, [
            'label' => 'Adresse postale :'
        ])
            ->add( 'email', null, [
            'label' => 'Adresse mail :'
        ])
            ->add( 'phone', null, [
            'label' => 'Téléphone :'
        ])
            ->add( 'role', ChoiceType::class, [
            'label' => 'Permissions :',
            'placeholder' => '---',
            'choices' => [
                'Client' => 0,
                'Particulier' => 1,
                'Administrateur' => 2,
            ],
        ])
            ->add('submit', SubmitType::class, [
            'label' => 'Valider'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
