<?php

namespace App\DataFixtures;

use DateTime;

use App\Entity\Billing;
use App\Entity\BillingLine;
use App\Entity\Booking;
use App\Entity\User;
use App\Entity\Pricing;
use App\Entity\Property;
use App\Entity\Type;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //User Table

            //Camping Admin User

                $user = new User();

                $user->setFirstname('Camping')
                ->setLastname("Espadrille Volante")
                ->setPassword($this->encoder->hashPassword($user, '0'))
                ->setAddress($faker->address)
                ->setEmail("espadrille-volante@camping.fr")
                ->setPhone($faker->phoneNumber)
                ->setRole(2);

                $this->setReference('USER0', $user);

                $manager->persist($user);

            //Generating other Users

                for ($i=1; $i<10; $i++) {

                    $user = new User();

                    $user->setFirstname($faker->firstName)
                    ->setLastname($faker->lastName)
                    ->setPassword($this->encoder->hashPassword($user, '0'))
                    ->setAddress($faker->address)
                    ->setEmail($faker->email)
                    ->setPhone($faker->phoneNumber)
                    ->setRole(rand(0,1));

                    $this->setReference('USER' . $i, $user);

                    $manager->persist($user);
                
                }

        //Type Table

        function typeFixture(

            string $name, 
            bool $isPrimary, 
            bool $isTax,
            string $fixture_ref,
            AppFixtures $fixture,
            ObjectManager $m)
        {
            $type = new Type();

            $type->setName($name)
            ->setIsPrimary($isPrimary)
            ->setIsTax($isTax);

            $fixture->setReference($fixture_ref, $type);
            $m->persist($type);

        }

        typeFixture('Mobile-Home',true,false,"TYPE1",$this,$manager);
        typeFixture('Caravane',true,false,"TYPE2",$this,$manager);
        typeFixture('Emplacement',true,false,"TYPE3",$this,$manager);
        typeFixture('Taxe',false,true,"TYPE4",$this,$manager);
        typeFixture('Optionnel',false,false,"TYPE5",$this,$manager);

        //Pricings Table

            $label_list = ['Mobile-Home 3p', 'Mobile-Home 4p', 'Mobile-Home 5p', 'Mobile-Home 6p', 
                'Caravane 2 places', 'Caravane 4 places', 'Caravane 6 places', 'Emplacement 8m²', 'Emplacement 12m²', 
                'Séjour Adulte', 'Séjour Enfant', 'Accès Piscine Adulte', 'Accès Piscine Enfant', 'Taxe Haute Saison', 'Remise Séjour'];
            $ref_list = ['MH3', 'MH4', 'MH5', 'MH68', 'C2P', 'C4P', 'C6P', 'E8M2', 'E12M2', 'TADUL', 'TENF', 'PADUL', 'PENF', 'THS%', 'REM%'];
            $price_list = [20, 24, 27, 34, 15, 18, 24, 12, 14, 0.35, 0.5, 1, 1.5, 15, 5]; //en euros

            function pricingFixture(

                string $reference, 
                string $label, 
                float $value,
                string $type_ref,
                string $fixture_ref,
                AppFixtures $fixture,
                ObjectManager $m)
            {
                $pricing = new Pricing();

                $pricing->setReference($reference)
                ->setLabel($label)
                ->setValue($value)
                ->setType($fixture->getReference($type_ref));

                $fixture->setReference($fixture_ref, $pricing);
                $m->persist($pricing);

            }

            //Mobile-homes

                for ($i=0; $i<4; $i++) {
                    pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE1','PRICE'.$i,$this,$manager);
                }

            //Caravans

                for ($i=4; $i<7; $i++) {
                    pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE2','PRICE'.$i,$this,$manager);
                }

            //Camping Slots

                for ($i=7; $i<9; $i++) {
                    pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE3','PRICE'.$i,$this,$manager);
                }

            //Taxes

                for ($i=9; $i<11; $i++) {
                    pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE4','PRICE'.$i,$this,$manager);
                }

            //Pool

                for ($i=11; $i<13; $i++) {
                    pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE5','PRICE'.$i,$this,$manager);
                }

            //Haute Saison et Remise

            for ($i=13; $i<15; $i++) {
                pricingFixture($ref_list[$i],$label_list[$i],$price_list[$i],'TYPE4','PRICE'.$i,$this,$manager);
            }

        //Properties Table

            //Camping mobile homes

                for ($i=0; $i<20; $i++) {

                    $property = new Property();

                    $property->setName('Mobile-Home '. $faker->name)
                    ->setAvailabilityStart(new DateTime('2021-05-05'))
                    ->setAvailabilityEnd(new DateTime('2021-10-10'))
                    ->setOwner($this->getReference('USER0'))
                    ->setPricing($this->getReference('PRICE' . rand(0,3) ) );

                    $this->setReference('PROP' . $i, $property);

                    $manager->persist($property);
                
                }

            //Private mobile homes

                for ($i=20; $i<50; $i++) {

                    $property = new Property();

                    $property->setName('Mobile-Home ' . $faker->name)
                    ->setAvailabilityStart($faker->dateTimeBetween('2021-05-05', '2021-06-20'))
                    ->setAvailabilityEnd($faker->dateTimeBetween('2021-06-21', '2021-10-10')) 
                    ->setOwner($this->getReference('USER' . rand(1,9)))
                    ->setPricing($this->getReference('PRICE' . rand(0,3) ) );

                    $this->setReference('PROP' . $i, $property);

                    $manager->persist($property);
                
                }

            //Caravans

                for ($i=50; $i<60; $i++) {

                    $property = new Property();

                    $property->setName('Caravane ' . $faker->name)
                    ->setAvailabilityStart($faker->dateTimeBetween('2021-05-05', '2021-06-20'))
                    ->setAvailabilityEnd($faker->dateTimeBetween('2021-06-21', '2021-10-10'))
                    ->setOwner($this->getReference('USER0'))
                    ->setPricing($this->getReference('PRICE' . rand(4,6)));

                    $this->setReference('PROP' . $i, $property);

                    $manager->persist($property);
                
                }

            //Slots

                for ($i=60; $i<90; $i++) {

                    $property = new Property();

                    $property->setName('Emplacement ' . $faker->name)
                    ->setAvailabilityStart($faker->dateTimeBetween('2021-05-05', '2021-06-20'))
                    ->setAvailabilityEnd($faker->dateTimeBetween('2021-06-21', '2021-10-10'))
                    ->setOwner($this->getReference('USER0'))
                    ->setPricing($this->getReference('PRICE' . rand(7,8)));

                    $this->setReference('PROP' . $i, $property);

                    $manager->persist($property);
                
                }
        
        //Bookings Table

            $cpt_bookings = 0;

            for ($i=0; $i<90; $i++) {

                $rng = rand(0,2);

                if ($rng > 0) {
                   
                    $booking = new Booking();

                    $date1 = $faker->dateTimeBetween('2021-05-05', '2021-06-20');
                    $date2 = $faker->dateTimeBetween('2021-06-21', '2021-10-10');
                    $days = $date2->diff($date1)->format("%a");

                    $adults = rand(1,4);
                    $children = rand(0,6);
    
                    $booking->setUser($this->getReference('USER'.rand(1,9)))
                    ->setProperty($this->getReference('PROP'. $i))
                    ->setDateStart($date1)
                    ->setDateEnd($date2)
                    ->setNbAdults($adults)
                    ->setNbChildren($children)
                    ->setPassPoolAdults($adults * rand(0,$days))
                    ->setPassPoolChildren($children * rand(0,$days));
    
                    $this->setReference('BOOK' . $cpt_bookings, $booking);

                    $cpt_bookings++;
    
                    $manager->persist($booking);

                }
            
            }

        //Billings Table

            for ($i=0; $i<$cpt_bookings; $i++) {

                $billing = new Billing();

                $billing->setDate(new DateTime())
                ->setUser($this->getReference('BOOK'.$i)->getUser())
                ->setOwner($this->getReference('BOOK'.$i)->getProperty()->getOwner());

                $this->setReference('BILL' . $i, $billing);

                $manager->persist($billing);
            
            }

        //BillingLines Table

            //Property

                for ($i=0; $i<$cpt_bookings; $i++) {

                    $rnd_ref = rand(0,8);

                    $billing_line = new BillingLine();

                    $billing_line->setBilling($this->getReference('BILL'. $i))
                    ->setLabel($label_list[$rnd_ref])
                    ->setReference($ref_list[$rnd_ref])
                    ->setNumber(1)
                    ->setPrice($price_list[$rnd_ref]);

                    $manager->persist($billing_line);
                
                }

            //Adult Tax

                for ($i=0; $i<$cpt_bookings; $i++) {

                    if ($this->getReference('BOOK' . $i)->getNbAdults() != 0) {

                        $billing_line = new BillingLine();

                        $date1 = $this->getReference('BOOK' . $i)->getDateStart();
                        $date2 = $this->getReference('BOOK' . $i)->getDateEnd();
                        $days = $date2->diff($date1)->format("%a");

                        $billing_line->setBilling($this->getReference('BILL'. $i))
                        ->setLabel($label_list[9])
                        ->setReference($ref_list[9])
                        ->setNumber($this->getReference('BOOK' . $i)->getNbAdults() * $days)
                        ->setPrice($price_list[9]);

                        $manager->persist($billing_line);

                    }
                }

            //Child Tax

                for ($i=0; $i<$cpt_bookings; $i++) {

                    if ($this->getReference('BOOK' . $i)->getNbChildren() != 0) {

                        $billing_line = new BillingLine();

                        $date1 = $this->getReference('BOOK' . $i)->getDateStart();
                        $date2 = $this->getReference('BOOK' . $i)->getDateEnd();
                        $days = $date2->diff($date1)->format("%a");

                        $billing_line->setBilling($this->getReference('BILL'. $i))
                        ->setLabel($label_list[10])
                        ->setReference($ref_list[10])
                        ->setNumber($this->getReference('BOOK' . $i)->getNbChildren() * $days)
                        ->setPrice($price_list[10]);

                        $manager->persist($billing_line);

                    }
                }

            //High Season

                $highSeason_start = new DateTime("2021-06-21");
                $highSeason_end = new DateTime ("2021-08-31");

            for ($i=0; $i<$cpt_bookings; $i++) {

                //Length of the stay
                    $date1 = $this->getReference('BOOK' . $i)->getDateStart();
                    $date2 = $this->getReference('BOOK' . $i)->getDateEnd();
                    $days = $date2->diff($date1)->format("%a");           
    
                switch (true) {
                    case ($date1 < $highSeason_start && $date2 > $highSeason_end) : //start before and end after
                        $highSeason_days = 71;
                        break ;
                    case ($date1 < $highSeason_start && $date2 < $highSeason_end) : //start before and end during
                        $highSeason_days = $date2->diff($highSeason_start)->format("%a");
                        break ;
                    case ($date1 > $highSeason_start && $date2 > $highSeason_end) : //start during and end after
                        $highSeason_days = $highSeason_end->diff($date1)->format("%a");
                        break ;
                    case ($date1 > $highSeason_start && $date2 < $highSeason_end) : //high season only
                        $highSeason_days = $days;
                        break ;
                    default : $highSeason_days = 0; break; //before & after high season
                }

                if ($highSeason_days != 0) {
                    
                    $billing_line = new BillingLine();

                    $billing_line->setBilling($this->getReference('BILL'. $i))
                    ->setLabel($label_list[13])
                    ->setReference($ref_list[13])
                    ->setNumber($highSeason_days)
                    ->setPrice($price_list[13]);

                    $manager->persist($billing_line);

                };
            };

            //Pool Only

                //Adults

                for ($i=0; $i<$cpt_bookings; $i++) {

                    if ($this->getReference('BOOK' . $i)->getNbAdults() != 0) {

                        $rng = rand(0,2);

                        if ($rng == 2) {

                            $billing_line = new BillingLine();

                            $billing_line->setBilling($this->getReference('BILL'. $i))
                            ->setLabel($label_list[11])
                            ->setReference($ref_list[11])
                            ->setNumber(rand(1,50))
                            ->setPrice($price_list[11]);

                            $manager->persist($billing_line);
                        }
                    }
                }

                //Children

                for ($i=0; $i<$cpt_bookings; $i++) {

                    if ($this->getReference('BOOK' . $i)->getNbChildren() != 0) {

                        $rng = rand(0,2);

                        if ($rng == 2) {

                            $billing_line = new BillingLine();

                            $billing_line->setBilling($this->getReference('BILL'. $i))
                            ->setLabel($label_list[12])
                            ->setReference($ref_list[12])
                            ->setNumber(rand(1,50))
                            ->setPrice($price_list[12]);

                            $manager->persist($billing_line);
                        }
                    }
                }

        //Sending data to manager

        $manager->flush();
    }
}
