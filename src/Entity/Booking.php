<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    public $date_start;

    /**
     * @ORM\Column(type="date")
     */
    public $date_end;

    /**
     * @ORM\Column(type="integer")
     */
    public $nb_adults;

    /**
     * @ORM\Column(type="integer")
     */
    public $nb_children;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\Column(type="integer")
     */
    private $pass_pool_adults;

    /**
     * @ORM\Column(type="integer")
     */
    private $pass_pool_children;

    public function __construct()
    {
        $this->billings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->date_start;
    }

    public function setDateStart(\DateTimeInterface $date_start): self
    {
        $this->date_start = $date_start;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end/* ->format('Y-m-d') */;

        return $this;
    }

    public function getNbAdults(): ?int
    {
        return $this->nb_adults;
    }

    public function setNbAdults(int $nb_adults): self
    {
        $this->nb_adults = $nb_adults;

        return $this;
    }

    public function getNbChildren(): ?int
    {
        return $this->nb_children;
    }

    public function setNbChildren(int $nb_children): self
    {
        $this->nb_children = $nb_children;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getPassPoolAdults(): ?int
    {
        return $this->pass_pool_adults;
    }

    public function setPassPoolAdults(int $pass_pool_adults): self
    {
        $this->pass_pool_adults = $pass_pool_adults;

        return $this;
    }

    public function getPassPoolChildren(): ?int
    {
        return $this->pass_pool_children;
    }

    public function setPassPoolChildren(int $pass_pool_children): self
    {
        $this->pass_pool_children = $pass_pool_children;

        return $this;
    }
}
