<?php

namespace App\Entity;

use App\Repository\BillingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BillingRepository::class)
 */
class Billing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="billings")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=BillingLine::class, mappedBy="billing")
     */
    private $billingLines;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="owner_billings")
     */
    private $owner;

    public function __construct()
    {
        $this->billingLines = new ArrayCollection();
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|BillingLine[]
     */
    public function getBillingLines(): Collection
    {
        return $this->billingLines;
    }

    public function addBillingLine(BillingLine $billingLine): self
    {
        if (!$this->billingLines->contains($billingLine)) {
            $this->billingLines[] = $billingLine;
            $billingLine->setBilling($this);
        }

        return $this;
    }

    public function removeBillingLine(BillingLine $billingLine): self
    {
        if ($this->billingLines->removeElement($billingLine)) {
            // set the owning side to null (unless already changed)
            if ($billingLine->getBilling() === $this) {
                $billingLine->setBilling(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

}
