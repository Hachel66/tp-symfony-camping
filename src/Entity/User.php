<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    public $firstname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    public $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @ORM\Column(type="integer")
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity=Property::class, mappedBy="owner")
     */
    private $properties;

    /**
     * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="user")
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity=Billing::class, mappedBy="user")
     */
    private $customer_billings;

    /**
     * @ORM\OneToMany(targetEntity=Billing::class, mappedBy="owner")
     */
    private $owner_billings;

    public function fullname()
    {
        return $this->firstname . " " . $this->lastname;
    }

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->customer_billings = new ArrayCollection();
        $this->owner_billings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRole(): ?int
    {
        return $this->role;
    }

    public function setRole(int $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setOwner($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getOwner() === $this) {
                $property->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setUser($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getUser() === $this) {
                $booking->setUser(null);
            }
        }

        return $this;
    }

    //Security Session

    public function getRoles()
    {
        switch ($this->role){
            case 2 : return ['ROLE_ADMIN'];
            case 1 : return ['ROLE_PART'];
            case 0 : return ['ROLE_CLIENT'];
        }
        
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password
        ]);
    }

    public function unserialize($serialized)
    {
        list($this->id, $this->email, $this->password) =
            unserialize($serialized, ["allowed_classes" => false]);
    }

    // public function getUserIdentifier()
    // {
    //     // TODO: getUserIdentifier()
    // }

    /**
     * @return Collection|Billing[]
     */
    public function getCustomerBillings(): Collection
    {
        return $this->customer_billings;
    }

    public function addBilling(Billing $customerBilling): self
    {
        if (!$this->customer_billings->contains($customerBilling)) {
            $this->customer_billings[] = $customerBilling;
            $customerBilling->setUser($this);
        }

        return $this;
    }

    public function removeBilling(Billing $customerBilling): self
    {
        if ($this->customer_billings->removeElement($customerBilling)) {
            // set the owning side to null (unless already changed)
            if ($customerBilling->getUser() === $this) {
                $customerBilling->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Billing[]
     */
    public function getOwnerBillings(): Collection
    {
        return $this->owner_billings;
    }

    public function addOwnerBilling(Billing $ownerBilling): self
    {
        if (!$this->owner_billings->contains($ownerBilling)) {
            $this->owner_billings[] = $ownerBilling;
            $ownerBilling->setOwner($this);
        }

        return $this;
    }

    public function removeOwnerBilling(Billing $ownerBilling): self
    {
        if ($this->owner_billings->removeElement($ownerBilling)) {
            // set the owning side to null (unless already changed)
            if ($ownerBilling->getOwner() === $this) {
                $ownerBilling->setOwner(null);
            }
        }

        return $this;
    }

}
