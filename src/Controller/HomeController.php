<?php
namespace App\Controller;

use App\Entity\Billing;
use App\Entity\BillingLine;
use App\Entity\Booking;
use App\Entity\Type;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\PropertyRepository;
use App\Repository\BookingRepository;
use App\Repository\PricingRepository;
use App\Repository\BillingRepository;
use App\Repository\BillingLineRepository;
use App\Repository\TypeRepository;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{
     /**
     * @var UserRepository
     */
    private $repoUser;

     /**
     * @var PropertyRepository
     */
    private $repoProp;

     /**
     * @var PricingRepository
     */
    private $repoPrice;

     /**
     * @var BookingRepository
     */
    private $repoBook;

     /**
     * @var BillingRepository
     */
    private $repoBill;

     /**
     * @var BillingLineRepository
     */
    private $repoLine;

     /**
     * @var TypeRepository
     */
    private $repoType;

    public function __construct(
        UserRepository $userRepository,
        PropertyRepository $propertyRepository,
        BookingRepository $bookingRepository,
        PricingRepository $pricingRepository, 
        BillingRepository $billingRepository,
        BillingLineRepository $lineRepository,
        TypeRepository $typeRepository)
    {
        $this->repoUser = $userRepository;
        $this->repoProp = $propertyRepository;
        $this->repoBook = $bookingRepository;
        $this->repoPrice = $pricingRepository;
        $this->repoBill = $billingRepository;
        $this->repoLine = $lineRepository;
        $this->repoType = $typeRepository;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="home"):
     */

    public function home(Request $request)
    {

        $form = $this->createFormBuilder()
            ->setAction('/catalogue')
            ->setMethod('POST')
            ->add('date_start', DateType::class, [
                'label'=> "Début du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('date_end', DateType::class, [
                'label'=> "Fin du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'label' => "Type : ",
                'placeholder' => 'Tout',
                'required' => false,
                'choice_label' => 'name',
                'multiple' => false,
                'query_builder' => function(TypeRepository $repoType){
                    return $repoType->filterType();
                }
            ])
            ->add('nb_adults', IntegerType::class, [
                'label' => 'Nb. d\'adultes : ',
                'attr' => [
                    'value' => 1,
                    'min' => 1,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('nb_children', IntegerType::class, [
                'label' => 'Nb. d\'enfants : ',
                'attr' => [
                    'value' => 0,
                    'min' => 0,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Rechercher'
            ])
            ->getForm();
        $form->handleRequest($request);

        return $this->render("home/home.html.twig", [
            'form' => $form->createView(),
            'user' => $this->getUser()
            ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        return $this->render("home/home.html.twig");
    }

    /**
     * @Route("/catalogue", methods={"GET", "POST"}, name="catalog"):
     */

    public function catalog(Request $request)
    {
        if ($request->get('form')) {
        $form = $this->createFormBuilder()
            ->add('date_start', DateType::class, [
                'label'=> "Début du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10',
                    'value' => $request->get('form')['date_start']
                ]
            ])
            ->add('date_end', DateType::class, [
                'label'=> "Fin du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10',
                    'value' => $request->get('form')['date_end']
                ]
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'label' => "Type : ",
                'placeholder' => 'Tout',
                'required' => false,
                'choice_label' => 'name',
                'multiple' => false,
                'query_builder' => function(TypeRepository $repoType){
                    return $repoType->filterType();},
                'attr' => [
                    'value' => $request->get('form')['type']
                ],
                
            ])
            ->add('nb_adults', IntegerType::class, [
                'label' => 'Nb. d\'adultes : ',
                'attr' => [
                    'value' => $request->get('form')['nb_adults'],
                    'min' => 1,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('nb_children', IntegerType::class, [
                'label' => 'Nb. d\'enfants : ',
                'attr' => [
                    'value' => $request->get('form')['nb_children'],
                    'min' => 0,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Rechercher'
            ])
            ->getForm();
        } else {
            $form = $this->createFormBuilder()
            ->add('date_start', DateType::class, [
                'label'=> "Début du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('date_end', DateType::class, [
                'label'=> "Fin du séjour : ",
                'widget' => 'single_text',
                'attr' => [
                    'min' => '2021-05-05',
                    'max' => '2021-10-10'
                ]
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'label' => "Type : ",
                'placeholder' => 'Tout',
                'required' => false,
                'choice_label' => 'name',
                'multiple' => false,
                'query_builder' => function(TypeRepository $repoType){
                    return $repoType->filterType();
                }
            ])
            ->add('nb_adults', IntegerType::class, [
                'label' => 'Nb. d\'adultes : ',
                'attr' => [
                    'value' => 1,
                    'min' => 1,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('nb_children', IntegerType::class, [
                'label' => 'Nb. d\'enfants : ',
                'attr' => [
                    'value' => 0,
                    'min' => 0,
                    'max' => 9,
                    'size' => 1
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Rechercher'
            ])
            ->getForm();
        }
        $form->handleRequest($request);

        $filters = null;

        if ($request->get('form')) {
            $filters = [
                'date_start' => $request->get('form')['date_start'],
                'date_end' => $request->get('form')['date_end'],
                'type' => $request->get('form')['type'],
                'nb_adults' => $request->get('form')['nb_adults'],
                'nb_children' => $request->get('form')['nb_children']
            ];
        };
          
        if ( $request->get('form') || ( $form->isSubmitted() && $form->isValid() ) ) { 

            $type = $request->get('form')['type'];
            if ($type == "") $type = null;

            $date_s = $request->get('form')['date_start'];
            $date_e = $request->get('form')['date_end'];

            $date_start = new DateTime($date_s);
            $date_end = new DateTime($date_e);
            
            $properties = $this->repoProp->filterProp($type, $date_start, $date_end);

        }
        else { $properties = $this->repoProp->findAll(); }
        
        return $this->render("home/catalog.html.twig", [
            'properties' => $properties,
            'form' => $form->createView(),
            'filters' => $filters,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/catalogue/{id}", methods={"GET","POST"}, name="prop-det"):
     */

    public function propDetail(Request $request, int $id)
    {
        $property = $this->repoProp->find($id);

        if ($request->get('filters')){
            $form = $this->createFormBuilder()
                ->setAction('/votre-reservation')
                ->setMethod('POST')
                ->add('date_start', DateType::class, [
                    'label' => "Début du séjour : ",
                    'widget' => 'single_text',
                    'attr' => [
                        'min' => $property->getAvailabilityStart()->format('Y-m-d'),
                        'max' => $property->getAvailabilityEnd()->format('Y-m-d'),
                        'value' => $request->get('filters')['date_start']
                    ]
                ])
                ->add('date_end', DateType::class, [
                    'label' => "Fin du séjour : ",
                    'widget' => 'single_text',
                    'attr' => [
                        'min' => $property->getAvailabilityStart()->format('Y-m-d'),
                        'max' => $property->getAvailabilityEnd()->format('Y-m-d'),
                        'value' => $request->get('filters')['date_end']
                    ]
                ])
                ->add('nb_adults', IntegerType::class, [
                    'label' => 'Nb. d\'adultes : ',
                    'attr' => [
                        'min' => 1,
                        'max' => 9,
                        'size' => 1,
                        'value' => $request->get('filters')['nb_adults']
                    ]
                ])
                ->add('nb_children', IntegerType::class, [
                    'label' => 'Nb. d\'enfants : ',
                    'attr' => [
                        'min' => 0,
                        'max' => 9,
                        'size' => 1,
                        'value' => $request->get('filters')['nb_children']
                    ]
                ])
                ->add('pool_adults', IntegerType::class, [
                    'label' => 'Accès piscine adulte(s) : ',
                    'attr' => [
                        'value' => 0,
                        'min' => 0
                    ]
                ])
                ->add('pool_children', IntegerType::class, [
                    'label' => 'Accès piscine enfant(s) : ',
                    'attr' => [
                        'value' => 0,
                        'min' => 0
                    ]
                ])
                ->add('property_id', HiddenType::class, [
                    'attr' => [
                        'value' => $id
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Effectuer la réservation'
                ])
                ->getForm();
        } else {
            $form = $this->createFormBuilder()
                ->setAction('/votre-reservation')
                ->setMethod('POST')
                ->add('date_start', DateType::class, [
                    'label' => "Début du séjour : ",
                    'widget' => 'single_text',
                    'attr' => [
                        'min' => $property->getAvailabilityStart()->format('Y-m-d'),
                        'max' => $property->getAvailabilityEnd()->format('Y-m-d')
                    ]
                ])
                ->add('date_end', DateType::class, [
                    'label' => "Fin du séjour : ",
                    'widget' => 'single_text',
                    'attr' => [
                        'min' => $property->getAvailabilityStart()->format('Y-m-d'),
                        'max' => $property->getAvailabilityEnd()->format('Y-m-d')
                    ]
                ])
                ->add('nb_adults', IntegerType::class, [
                    'label' => 'Nb. d\'adultes : ',
                    'attr' => [
                        'min' => 1,
                        'max' => 9,
                        'size' => 1,
                        'value' => 1
                    ]
                ])
                ->add('nb_children', IntegerType::class, [
                    'label' => 'Nb. d\'enfants : ',
                    'attr' => [
                        'min' => 0,
                        'max' => 9,
                        'size' => 1,
                        'value' => 0
                    ]
                ])
                ->add('pool_adults', IntegerType::class, [
                    'label' => 'Accès piscine adulte(s) : ',
                    'attr' => [
                        'value' => 0,
                        'min' => 0
                    ]
                ])
                ->add('pool_children', IntegerType::class, [
                    'label' => 'Accès piscine enfant(s) : ',
                    'attr' => [
                        'value' => 0,
                        'min' => 0
                    ]
                ])
                ->add('property_id', HiddenType::class, [
                    'attr' => [
                        'value' => $id
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Effectuer la réservation'
                ])
                ->getForm();
        }
        $form->handleRequest($request);

        return $this->render("home/prop-detail.html.twig", [
            'property' => $property, 
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/votre-reservation", methods={"POST"}, name="mybooking"):
     */

    public function myBooking(Request $request, UserPasswordHasherInterface $encoder)
    {

        //Previous page POST Info
            $property_id = $request->get('form')['property_id'];
            $date_start = $request->get('form')['date_start'];
            $date_end = $request->get('form')['date_end'];
            $nb_adults = $request->get('form')['nb_adults'];
            $nb_children = $request->get('form')['nb_children'];
            $pool_adults = $request->get('form')['pool_adults'];
            $pool_children = $request->get('form')['pool_children'];

        //Selected Property Info
            $property = $this->repoProp->find($property_id);
            $prop_value = $property->getPricing()->getValue(); 
            $prop_label = $property->getPricing()->getLabel();
            $prop_ref = $property->getPricing()->getReference();
        
        //MyBooking Info
            $booking = [
                'property_id' => $property_id,
                'date_start' => $date_start,
                'date_end' => $date_end,
                'nb_adults' => $nb_adults,
                'nb_children' => $nb_children,
                'pass_adults' =>  $pool_adults,
                'pass_children' =>  $pool_children
            ];
        //Pricings List
            $pricings = $this->repoPrice->findAll();

        //Length of the stay
            $date1 = new DateTime($date_start);
            $date2 = new DateTime($date_end);
            $days = $date2->diff($date1)->format("%a") +1;

        //High Season
            $highSeason_start = new DateTime("2021-06-21");
            $highSeason_end = new DateTime ("2021-08-31");

            switch (true) {
                case ($date1 <= $highSeason_start && $date2 >= $highSeason_end) : //start before and end after
                    $highSeason_days = 71;
                    break ;
                case ($date1 <= $highSeason_start && $date2 <= $highSeason_end) : //start before and end during
                    $highSeason_days = $date2->diff($highSeason_start)->format("%a") +1;
                    break ;
                case ($date1 >= $highSeason_start && $date2 >= $highSeason_end) : //start during and end after
                    $highSeason_days = $highSeason_end->diff($date1)->format("%a") +1;
                    break ;
                case ($date1 >= $highSeason_start && $date2 <= $highSeason_end) : //high season only
                    $highSeason_days = $days;
                    break ;
                default : $highSeason_days = 0; break; //before & after high season
            }

        //Subtotals
            $subtotals = [];

            array_push($subtotals, $prop_value); //Property Price

            array_push($subtotals, $pricings[9]->getValue() * $nb_adults * $days ); //Adult Tax

            if ($nb_children != 0) {
                array_push($subtotals, $pricings[10]->getValue() * $nb_children * $days); //Children Tax
            }

            if ($pool_adults != 0) {
                array_push($subtotals, $pricings[11]->getValue() * $pool_adults); //Pool Pass Adult
            }

            if ($pool_children != 0) {
                array_push($subtotals, $pricings[12]->getValue() * $pool_children); //Pool Pass Children
            }

            if ($highSeason_days != 0) {

                $stay_taxes = 0;

                switch (true) {
                    case ($nb_adults != 0) : $stay_taxes += $pricings[9]->getValue() * $nb_adults * $highSeason_days;
                    case ($nb_children != 0) : $stay_taxes += $pricings[10]->getValue() * $nb_children * $highSeason_days;
                }

                $highSeason_tax = $stay_taxes * $pricings[13]->getValue() /100;

                array_push($subtotals, $highSeason_tax); //High Season Tax
            }

        //Totals
            $totals = [0,0];

            foreach ($subtotals as $subtotal) {
                $totals[0] += $subtotal;
            }

            //With Discount
            if ($days >= 7) {
                $totals[1] = $totals[0] * (100 - $pricings[13]->getValue()) /100;
            }

            //With TVA

            if ($totals[1] != 0) {
                $totals[2] = $totals[1] * 1.2;
            } else {
                $totals[2] = $totals[0] * 1.2;
            }


        //Billing User Form
            $userform = $this->createFormBuilder()
                ->add('firstname', TextType::class, [
                    'label' => 'Prénom : '
                ])
                ->add('lastname', TextType::class, [
                    'label' => 'Nom : '      
                ])
                ->add('address', TextType::class, [
                    'label' => 'Adresse'
                ])
                ->add('email', TextType::class, [
                    'label' => 'Email : '      
                ])
                ->add('phone', TextType::class, [
                    'label' => 'Téléphone : '      
                ])
        //Hidden hydratation of previous page data
                ->add('property_id', HiddenType::class, [
                    'attr' => [
                        'value' => $property_id
                    ]
                ])
                ->add('date_start', HiddenType::class, [
                    'attr' => [
                        'value' => $date_start
                    ]
                ])
                ->add('date_end', HiddenType::class, [
                    'attr' => [
                        'value' => $date_end
                    ]
                ])
                ->add('nb_adults', HiddenType::class, [
                    'attr' => [
                        'value' => $nb_adults
                    ]  
                ])
                ->add('nb_children', HiddenType::class, [
                    'attr' => [
                        'value' => $nb_children 
                    ]    
                ])
                ->add('pool_adults', HiddenType::class, [
                    'attr' => [
                        'value' => $pool_adults
                    ]
                ])
                ->add('pool_children', HiddenType::class, [
                    'attr' => [
                        'value' => $pool_children
                    ]
                ])
        //All Data Form Submit
            ->add('submit', SubmitType::class, [
                'label' => 'Réserver et Payer'
            ])
            ->getForm();
            $userform->handleRequest($request);

        if ($userform->isSubmitted() && $userform->isValid() && isset($request->get('form')['email'])) {
        
            $manager = $this->getDoctrine()->getManager();

            //Creating new User
            
                $u = new User;

                $u->setFirstname($request->get('form')['firstname'])
                ->setLastname($request->get('form')['lastname'])
                ->setPassword($encoder->hashPassword($u, '0'))
                ->setAddress($request->get('form')['address'])
                ->setEmail($request->get('form')['email'])
                ->setPhone($request->get('form')['phone'])
                ->setRole(0);

                $manager->persist($u);
                $manager->flush();

            //Creating new Booking

                $book = new Booking;

                $book->setUser($u)
                ->setProperty($property)
                ->setDateStart(new DateTime($booking['date_start']))
                ->setDateEnd(new DateTime($booking['date_end']))
                ->setNbAdults($booking['nb_adults'])
                ->setNbChildren($booking['nb_children'])
                ->setPassPoolAdults($booking['pass_adults'])
                ->setPassPoolChildren($booking['pass_children']);

                $manager->persist($book);

            //Creating new Billing

                $billing = new Billing();

                $billing->setDate(new DateTime())
                ->setUser($u)
                ->setOwner($property->getOwner());

                $manager->persist($billing);

            //Creating new Billing Lines

                //Property Price

                    $billing_line = new BillingLine();

                    $billing_line->setBilling($billing)
                    ->setLabel($prop_label)
                    ->setReference($prop_ref)
                    ->setNumber(1)
                    ->setPrice($prop_value);

                    $manager->persist($billing_line);

                //Adult Tax

                    if ($booking['nb_adults'] != 0) {

                        $billing_line = new BillingLine();

                        $billing_line->setBilling($billing)
                        ->setLabel($pricings[9]->getLabel())
                        ->setReference($pricings[9]->getReference())
                        ->setNumber(($booking['nb_adults']*$days))
                        ->setPrice($pricings[9]->getValue());

                        $manager->persist($billing_line);
                    }

                //Children Tax

                    if ($booking['nb_children'] != 0) {

                        $billing_line = new BillingLine();

                        $billing_line->setBilling($billing)
                        ->setLabel($pricings[10]->getLabel())
                        ->setReference($pricings[10]->getReference())
                        ->setNumber(($booking['nb_children']*$days))
                        ->setPrice($pricings[10]->getValue());

                        $manager->persist($billing_line);
                    }

                //Pool Pass Adult

                    if ($booking['pass_adults'] != 0) {

                        $billing_line = new BillingLine();

                        $billing_line->setBilling($billing)
                        ->setLabel($pricings[11]->getLabel())
                        ->setReference($pricings[11]->getReference())
                        ->setNumber($booking['pass_adults'])
                        ->setPrice($pricings[11]->getValue());

                        $manager->persist($billing_line);
                    }

                //Pool Pass Children

                    if ($booking['pass_children'] != 0) {

                        $billing_line = new BillingLine();

                        $billing_line->setBilling($billing)
                        ->setLabel($pricings[12]->getLabel())
                        ->setReference($pricings[12]->getReference())
                        ->setNumber($booking['pass_children'])
                        ->setPrice($pricings[12]->getValue());

                        $manager->persist($billing_line);
                    }

                //High Season

                    if ($highSeason_days != 0) {

                        $billing_line = new BillingLine();

                        $billing_line->setBilling($billing)
                        ->setLabel($pricings[13]->getLabel())
                        ->setReference($pricings[13]->getReference())
                        ->setNumber($highSeason_days)
                        ->setPrice($pricings[13]->getValue());

                        $manager->persist($billing_line);
                    }

            //Send all data
            
            $manager->flush();

            echo "<script>alert(\"Nous vous remercions de votre réservation ! Redirection vers l\'accueil\")</script>";
            return $this->redirectToRoute('home');

        }


        return $this->render("home/my-booking.html.twig", [
            'property' => $property,
            'pricings' => $pricings,
            'booking'=> $booking,
            'days' => $days,
            'totals' => $totals,
            'highSeason_days' => $highSeason_days + 1,
            'highSeason_tax' => $highSeason_tax,
            'userform' => $userform->createView(),
            'user' => $this->getUser()
            ]);

    }

}
