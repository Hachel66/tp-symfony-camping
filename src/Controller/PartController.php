<?php
namespace App\Controller;

use App\Repository\BillingRepository;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class PartController extends AbstractController
{
     /**
     * @var PropertyRepository
     */
    private $repoProp;

     /**
     * @var BillingRepository
     */
    private $repoBill;

    public function __construct(
        PropertyRepository $propertyRepository,
        BillingRepository $billingRepository)
    {
        $this->repoProp = $propertyRepository;
        $this->repoBill = $billingRepository;
    }

     /**
     * @Route("/part/mes-annonces", methods={"GET"}, name="myproperties"):
     */

    public function myProperties()
    {
        $properties = $this->repoProp->findBy(
            ['owner' => $this->getUser()->getId()]
        );
        return $this->render("part/my-properties.html.twig", [
            'properties' => $properties,
            'user' => $this->getUser()
        ]);
    }   

    /**
     * @Route("/part/mes-factures/", methods={"GET"}, name="my-bill"):
     */
    public function myBillingList()
    {
        $bills = $this->repoBill->findBillingByOwner($this->getUser()->getId());
        return $this->render("part/my-billings.html.twig", [
            'bills' => $bills,
            'user' => $this->getUser()
        ]);
    }
    
   
}