<?php

namespace App\Controller;

use App\Entity\Property;
use App\Entity\User;
use App\Form\PropertyType;
use App\Form\UserType;
use App\Repository\BillingLineRepository;
use App\Repository\BillingRepository;
use App\Repository\BookingRepository;
use App\Repository\PricingRepository;
use App\Repository\PropertyRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends AbstractController
{
     /**
     * @var UserRepository
     */
    private $repoUser;

     /**
     * @var BookingRepository
     */
    private $repoBook;

     /**
     * @var BillingRepository
     */
    private $repoBill;

     /**
     * @var BillingLineRepository
     */
    private $repoLine;

     /**
     * @var PricingLineRepository
     */
    private $repoPrice;

   public function __construct(
       UserRepository $userRepository,
       BookingRepository $bookingRepository,
       BillingRepository $billingRepository,
       BillingLineRepository $lineRepository,
       PricingRepository $pricingRepository)
   {
       $this->repoUser = $userRepository;
       $this->repoBook = $bookingRepository;
       $this->repoBill = $billingRepository;
       $this->repoLine = $lineRepository;
       $this->repoPrice = $pricingRepository;
   }

     /**
     * @Route("/admin/factures", methods={"GET"}, name="bill"):
     */

    public function billingList()
    {
        $bills = $this->repoBill->findAll();
        return $this->render("admin/billings.html.twig", [
            'bills' => $bills,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/admin/factures/{id}", methods={"GET"}, name="bill-det"):
     */

    public function billingDetail(int $id)
    {
        $camping = $this->repoUser->find(1);

        $booking = $this->repoBook->findBooking2($id)[0];
        
        $pricings = $this->repoPrice->findAll();
        
        $lines = $this->repoLine->findBy(['billing' => $id]);

        $date1 = $booking->getDateStart();
        $date2 = $booking->getDateEnd();
        $days = $date2->diff($date1)->format("%a") +1;

        //Subtotals
        $subtotals = [];
        $totals = [0,0];

        foreach ($lines as $line) {

            if ($line->getReference() != "THS%") {
                array_push($subtotals, $line->getPrice() * $line->getNumber());

            } else {

                $stay_taxes = 0;

                switch (true) {
                    case ($booking->getNbAdults() != 0) : $stay_taxes += $pricings[9]->getValue() * $booking->getNbAdults() * $line->getNumber();
                    case ($booking->getNbChildren() != 0) : $stay_taxes += $pricings[10]->getValue() * $booking->getNbChildren() * $line->getNumber();
                }

                $highSeason_tax = $stay_taxes * $pricings[13]->getValue() /100;

                array_push($subtotals, $highSeason_tax); //High Season Tax
            }
        }

       //Totals
       $totals = [0,0];

       foreach ($subtotals as $subtotal) {
           $totals[0] += $subtotal;
       }

       //With Discount
       if ($days >= 7) {
           $totals[1] = $totals[0] * (100 - $pricings[13]->getValue()) /100;
       }

       //With TVA

       if ($totals[1] != 0) {
           $totals[2] = $totals[1] * 1.2;
       } else {
           $totals[2] = $totals[0] * 1.2;
       }

        $bill = $this->repoBill->find($id);

        return $this->render("admin/billing-detail.html.twig", [
            'camping' => $camping,
            'booking'=> $booking,
            'days' => $days,
            'bill' => $bill,
            'lines' => $lines,
            'totals' => $totals,
            'highSeason_tax' => $highSeason_tax,
            'pricings' => $pricings,
            'user' => $this->getUser()
            ]);
    }

    /**
     * @Route("/admin", methods={"GET"}, name="admin"):
     */

    public function admin(Request $request)
    {
        return $this->render("admin/admin.html.twig", [
            'user' => $this->getUser()
            ]);
    }

    /**
     * @Route("/admin/editeur-propriete/{id}", name="prop-editor", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     */
    public function propEditor(int $id = -1, PropertyRepository $propRepo,
                                   Request $request)
    {
        if($id == -1){
            $prop = new Property();
        } else {
            $prop = $propRepo->find($id);
        }

        $form = $this->createForm(PropertyType::class, $prop);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($prop);
            $manager->flush();
            if ($id == -1) {
                echo "<script>alert(\"La propriété a bien été ajoutée !\")</script>";
                return $this->redirectToRoute('admin');
            } else {
                echo "<script>alert(\"La propriété a bien été modifiée !\")</script>";
                return $this->redirectToRoute('prop-det', ['id' => $id]);
            }
            
        }

        return $this->render("admin/add-prop.html.twig", [
            'id' => $id,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/admin/editeur-utilisateur/{id}", name="user-editor", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     */
    public function userEditor(int $id = -1, UserRepository $userRepo,
                                   Request $request, AuthenticationUtils $authenticationUtils)
    {
        if($id == -1) {
            $user = new User();
        } else {
            $user = $userRepo->find($id);
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();
            if ($id == -1) {
                echo "<script>alert(\"L'utilisateur a bien été ajouté !\")</script>";
            } else {
                echo "<script>alert(\"L'utilisateur a bien été modifié !\")</script>";
            }
            return $this->redirectToRoute('user-list');
        }

        return $this->render("admin/add-user.html.twig", [
            'id' => $id,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/admin/utilisateurs", name="user-list", methods={"GET"})
     *
     * @param Request $request
     *
     */

    public function userList()
    {
        $members = $this->repoUser->findAll();
        return $this->render("admin/users.html.twig", [
            'members' => $members,
            'user' => $this->getUser()
        ]);
    }
}
