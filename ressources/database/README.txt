Pour avoir la base de données, veuillez rentrer une des commandes ci-dessous dans la console de votre choix (lando/docker requis) :

> lando console make:migration ; lando console d:m:m ; lando console d:f:l (Visual Studio)
> lando console make:migration && lando console d:m:m && lando console d:f:l (PowerShell)

